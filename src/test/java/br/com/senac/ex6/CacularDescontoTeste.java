/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.senac.ex6;

import org.junit.Test;
import static org.junit.Assert.*;

public class CacularDescontoTeste {

    @Test
    public void testDesconto() {
       
        Valores vl = new Valores(100, 5, 1000);
        double rest = CalcularDesconto.getPreco(vl);
        assertEquals(490, rest, 0.1);
    }
}
