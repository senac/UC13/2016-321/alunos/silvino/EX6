package br.com.senac.ex6;

/**
 *
 * @author Administrador
 */

public class Valores {

    private double precoItem;
    private double quantidade;
    private double fatorDesconto;
    private double limite;

    public Valores(double precoIten, double quantidade, double limite) {
        this.precoItem = precoItem;
        this.quantidade = quantidade;

        this.limite = limite;
    }

    public double getPrecoItem() {
        return precoItem;
    }

    public void setPrecoItem(double precoIten) {
        this.precoItem = precoIten;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getFatorDesconto() {
        return fatorDesconto;
    }

    public void setFatorDesconto(double fatorDesconto) {
        this.fatorDesconto = fatorDesconto;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

}
